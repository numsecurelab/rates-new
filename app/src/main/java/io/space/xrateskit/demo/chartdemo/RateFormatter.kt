package io.space.xrateskit.demo.chartdemo

import io.space.chartview.ChartView
import io.space.xrateskit.demo.App
import io.space.xrateskit.demo.chartdemo.entities.Currency
import io.space.xrateskit.demo.chartdemo.entities.CurrencyValue
import java.math.BigDecimal

class RateFormatter(private val currency: Currency) : ChartView.RateFormatter {
    override fun format(value: BigDecimal, maxFraction: Int?): String? {
        val currencyValue = CurrencyValue(currency, value)

        return App.numberFormatter.formatForRates(currencyValue, maxFraction = maxFraction, trimmable = false)
    }
}
