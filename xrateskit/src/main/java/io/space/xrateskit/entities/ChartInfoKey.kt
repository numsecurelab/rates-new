package io.space.xrateskit.entities

data class ChartInfoKey(
        val coin: String,
        val currency: String,
        val chartType: ChartType
)

